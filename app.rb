require 'rubygems'
require 'bundler/setup'

# Bundler.require(:default)

require 'sinatra/base'
require 'sinatra/json'
require 'rack/gc_tracer'
require 'pry-byebug'

# GC::Tracer.start_logging '/tmp/gc.log', rusage: true, events: %i(end_sweep)


#
# Thread.new do
#
#   loop do
#     10000.times do
#       Random.new.bytes(10)
#     end
#     GC::Tracer.flush_logging
#     sleep 1
#
#   end
# end



class App < Sinatra::Base

  get '/' do

    erb :index
  end

  get '/2' do

    erb :index_2
  end

  #
  # get '/data' do
  #   file = File.open('/tmp/gc.log', 'r')
  #   metrics = %w(tick ru_maxrss old_objects total_allocated_objects total_freed_objects heap_live_slots)
  #
  #
  #   line = file.readline.split("\t")
  #   indexes = metrics.map {|m| line.index(m)}
  #
  #   data = []
  #
  #   file.readlines.each do |line|
  #     line = line.split("\t")
  #     data << indexes.map { |i| line[i].to_i }
  #   end
  #
  #   file.close
  #
  #   json data
  # end

  get '/data' do
    file = File.open('logs/1.log', 'r')
    data = []
    file.readlines.each do |line|
      line = line.split(" ")
      data << line
    end
    file.close
    json data
  end

  get '/data_tmp' do
    file = File.open('logs/mylog2.log', 'r')
    data = []
    file.readlines.each do |line|
      line = line.split(" ")
      data << line
    end
    file.close
    json data
  end


  get '/data2' do
    file = File.open('logs/2.log', 'r')
    data = []
    file.readlines.each do |line|
      line = line.split(" ")
      data << line
    end
    file.close
    json data
  end

  get '/data3' do
    file = File.open('logs/3.log', 'r')
    data = []
    file.readlines.each do |line|
      line = line.split(" ")
      data << line
    end
    file.close
    json data
  end

  get '/metric_list' do
    File.open('/tmp/gc.log', 'r') do |file|
      @line = file.readline.split("\t")
    end

    json @line
  end

  run!
end
