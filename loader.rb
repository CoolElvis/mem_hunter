require 'faraday'

module Loader
  module_function

  def run(host, path_to_params)
    file = File.open(path_to_params)
    conn = Faraday.new(url: host,)
    file.readlines.each do |line|
     p line = line.gsub('"', '').strip
      conn.get(line)
    end
  end

end

module Dumper
  module_function

  def dump(pid, file_name)
    `rbtrace -p "#{pid}" -e 'Thread.new{GC.start;require "objspace";File.open("#{file_name}", "w"){|f| ObjectSpace.dump_all(output: f) }}'`
  end

end




# Dumper.dump(21794, 'dump_3.json')
Loader.run('http://localhost:3000', './logs/api.log')

